
//
// Project       : UART core
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : uart_tx.sv
// Description   : UART transmitter with different input data width
//                 (!) input data width should be divided by 8
//

module uart_tx
  #(
    parameter int pW_DAT = 8 // should be divided by 8
  )
  (
    input                iclk     ,
    input                iclk_ena , // UART clock
    input                irst     ,
    input                iena     ,
    input   [pW_DAT-1:0] idat     , // Word (Nx8 bit)
    //
    output logic         oreq     , // Transmitter rdy for next word
    output logic         otx        // UART TX line
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  localparam int pWORDS = (pW_DAT / 8) - 1;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic               [3:0] cnt;
  logic        [pW_DAT-1:0] fdat;
  logic              [10:0] freg;
  logic  [$clog2(pWORDS):0] cnt_word;

  enum logic [2:0] {
    StWait = 3'd0,
    StSOP  = 3'd1,
    StDAT  = 3'd2,
    StEOP  = 3'd3,
    StEnd  = 3'd4
  } St;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    if (irst)  St <= StWait;
    else case (St)
      StWait    : if (iena)                    St <= StSOP;
      StSOP     : if (iclk_ena)                St <= StDAT;
      StDAT     : if (cnt == 'd10 & iclk_ena)  St <= StEOP;
      StEOP     : if (cnt_word == pWORDS)      St <= StEnd;
                  else                         St <= StSOP;
      StEnd     :                              St <= StWait;
    endcase
  end

  always_ff@(posedge iclk) begin
    // Word register
    if (iena)              fdat <= idat;
    else if (St == StEOP)  fdat <= fdat << 8 | 8'h00;
    // Bit counter
    if (St == StSOP)       cnt <= '0;
    else if (iclk_ena)     cnt <= cnt + 1'b1;
    // One word forming
    if (St == StSOP)       freg <= {2'b11, fdat[$high(fdat):$high(fdat)-7], 1'b0}; // 2xSTOP, DATA, START
    else if (iclk_ena)     freg <= freg >> 1;
    // Words counter
    if (St == StWait)      cnt_word <= '0;
    else if (St == StEOP)  cnt_word <= cnt_word + 1'b1;
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    if (irst)  oreq <= '0;
    else       oreq <= (St == StWait) & iclk_ena;

    if (St == StDAT)  otx <= freg[$low(freg)];
    else              otx <= 1'b1;
  end

endmodule
