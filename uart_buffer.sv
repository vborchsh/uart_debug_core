
//
// Project       : -
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : uart_buffer.sv
// Description   : UART debug core buffer with different memory depth and width
//

module uart_buffer
  #(
    parameter int pW_ADR = 8 ,
    parameter int pW_DAT = 8
  )
  (
    input                     irst   ,
    input                     iclk   ,
    //
    input                     iena   ,
    input        [pW_DAT-1:0] idat   ,
    input                     ireq   ,
    //
    output logic              oena   ,
    output logic [pW_DAT-1:0] odat   ,
    output logic              oempty ,
    output logic              ofull
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  localparam int pADR = 2**pW_ADR;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic    [pW_ADR-1:0] wr_addr;
  logic    [pW_ADR-1:0] rd_addr;
  logic    [pW_DAT-1:0] ram_mem [pADR];
  logic                 req;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  assign req = ireq & ~oempty;
  // Wr RAM
  always_ff@(posedge iclk) begin
    if (irst)                   wr_addr <= '0;
    else if (iena & ~&wr_addr)  wr_addr <= wr_addr + 1'b1;

    if (~&wr_addr)              ram_mem[wr_addr] <= idat;
  end

  // Rd RAM
  always_ff@(posedge iclk) begin
    if (irst)                  rd_addr <= '0;
    else if (req & ~&rd_addr)  rd_addr <= rd_addr + 1'b1;

    odat <= ram_mem[rd_addr];
    oena <= req;
  end

  assign oempty = (wr_addr == '0) | (&rd_addr);
  assign ofull  = (&wr_addr) & (rd_addr == '0);

endmodule
