
//
// Project       : UART core
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : uart_engine_pure.sv
// Description   : UART core top module without debug buffer
//

/*
  logic                      uart__iclk            ;
  logic                      uart__iclk_ena        ;
  logic                      uart__irst            ;
  logic                      uart__irx             ;
  logic                      uart__otx             ;
  logic                      uart__iena            ;
  logic    [pUART_W_DAT-1:0] uart__idat            ;
  logic                      uart__oreq            ;
  logic                      uart__oena            ;
  logic                [7:0] uart__odat            ;

  assign uart__iclk            = ;
  assign uart__iclk_ena        = ;
  assign uart__irst            = ;
  assign uart__irx             = ;
  assign uart__iena            = ;
  assign uart__idat            = ;

  uart_engine
  uart__
  (
    . iclk                           (uart__iclk                          ) ,
    . iclk_ena                       (uart__iclk_ena                      ) ,
    . irst                           (uart__irst                          ) ,
    . irx                            (uart__irx                           ) ,
    . otx                            (uart__otx                           ) ,
    . iena                           (uart__iena                          ) ,
    . idat                           (uart__idat                          ) ,
    . oreq                           (uart__oreq                          ) ,
    . oena                           (uart__oena                          ) ,
    . odat                           (uart__odat                          )
  );
*/
module uart_engine_pure
  #(
    parameter int pW_DAT = 24  ,
    parameter int pT_CLK = "ext"
  )
  (
    input                iclk     , // Internal clock
    input                iclk_ena , // UART clock enable signal
    input                irst     ,
    // UART
    input                irx      , // UART RX line
    output logic         otx      , // UART TX line
    //
    input                iena     ,
    input   [pW_DAT-1:0] idat     ,
    output logic         oreq     ,
    output logic         oena     ,
    output logic   [7:0] odat
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  logic              tx__iclk    ;
  logic              tx__iclk_ena;
  logic              tx__irst    ;
  logic              tx__iena    ;
  logic [pW_DAT-1:0] tx__idat    ;
  logic              tx__oreq    ;
  logic              tx__otx     ;

  logic              rx__iclk    ;
  logic              rx__irst    ;
  logic              rx__irx     ;
  logic              rx__oena    ;
  logic       [7:0]  rx__odat    ;  

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    otx  <= tx__otx;
    oena <= rx__oena;
    odat <= rx__odat;
    oreq <= tx__oreq;
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------
  
  assign tx__iclk     = iclk;
  assign tx__iclk_ena = iclk_ena;
  assign tx__irst     = irst;
  assign tx__iena     = iena;
  assign tx__idat     = idat;

  assign rx__iclk     = iclk;
  assign rx__irst     = irst;
  assign rx__irx      = irx;

  uart_tx
  #(
    . pW_DAT    (pW_DAT               )
  )
  tx__
  (
    . iclk      (tx__iclk             ) ,
    . iclk_ena  (tx__iclk_ena         ) ,
    . irst      (tx__irst             ) ,
    . iena      (tx__iena             ) ,
    . idat      (tx__idat             ) ,
    . oreq      (tx__oreq             ) ,
    . otx       (tx__otx              )
  );

  uart_rx
  #(
    . pT_CLK    (pT_CLK               )
  )
  rx__
  (
    . iclk      (rx__iclk             ) ,
    . irst      (rx__irst             ) ,
    . irx       (rx__irx              ) ,
    . oena      (rx__oena             ) ,
    . odat      (rx__odat             )
  );

endmodule
