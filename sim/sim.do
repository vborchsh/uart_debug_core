# Usage:
# do sim.do sim;         - libraries recompilation
# do sim.do; run X us;   - restart & run on X time

.main clear

quietly set RTL_DIR "./../"
quietly set TB_DIR "."
quietly set flag_restart 1

# Argument parsing
if { $argc < 1 } {
  puts "Restarting..."
} elseif { $1 == "sim" } {
  puts "Recompiling..."
  set flag_restart 0
}

vlib work
# Update local files
vlog +initreg+0 +initmem+0 -work work -sv -mfcu $RTL_DIR/*.sv
vlog +initreg+0 +initmem+0 -work work -sv -mfcu $TB_DIR/*.sv

# If recompiling - recreate all libraries. Else - just restart
if { $flag_restart == 1 } {
  restart -all -force
} elseif { $flag_restart == 0} {
  vsim -suppress 7034 +initreg+0 +initmem+0 -novopt -voptargs="+acc" -L work work.tb_uart -t 100ps
}
