
//
// Project       : Cyclone V locator board
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : tb_uart.sv
// Description   : Testbench for UART debug core
//

module tb_uart();
  timeunit 1ns;
  timeprecision 1ns;

  //--------------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------------

  logic        iclk     ; // Internal clock
  logic        iclk_ena ; // UARTx2 clk_ena
  logic        irst     ;
  logic        ireq     ;
  logic        irx      ; // UART RX line
  logic        otx      ; // UART TX line
  logic        iena     ;
  logic        icena    ;
  logic [23:0] idat     ;
  logic [23:0] icdat    ;
  logic        oena     ;
  logic  [7:0] odat     ;

  logic [15:0] cnt_ena;

  //--------------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------------

  // Clock
  initial forever #1 iclk++;
  // 115200 UART clock
  always@(posedge iclk)  begin
    if (cnt_ena == 3124)  cnt_ena <= '0;
    else                  cnt_ena <= cnt_ena + 1'b1;
    iclk_ena <= (cnt_ena == (3124));
  end
  
  // Reset
  initial begin
    irst=1; #4; irst=0; #2;
    @(posedge iclk);
    #2ms;
    ireq = 1'b1;
    @(posedge iclk);
    ireq = 1'b0;
  end

  // Input data thread
  initial begin
    #1us;
    iena = 1'b1;
    repeat(260) begin
      idat = $random();
      @(posedge iclk);
    end
  end

  // Loopback
  assign irx = otx;

  //--------------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------------

  uart_engine
  dut__
  (
    .*
  );

endmodule

