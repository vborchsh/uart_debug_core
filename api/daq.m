% Read data from debug core

%% Cleaning workspace
clear all;
close all;
clc;

%% Data acquisition
% Close all COM ports, if it open
ports = instrfind;
if (~isempty(ports))
    fclose(ports);
    delete(ports);
end

% Setup COM port parameters
pBR       = 38400;
pPORTNAME = '/dev/ttyUSB0';
pBUFFER   = ((16384-2) * 3); % [bytes] Should be agreed with FPGA firmware. For v0.0.1 16384 samples x 24 bit
pTIMEOUT  = 2 * (pBUFFER / (pBR / 8)); % [sec] Should be agreed with data size & port speed

%% Data acquisition

% Create terminal object
s = serial(pPORTNAME, 'BaudRate', pBR, 'DataBits', 8, 'StopBit', 1, 'Parity', 'none', 'FlowControl', 'none');
% Some setup
s.ByteOrder        = 'BigEndian'; 
s.InputBufferSize  = pBUFFER; 
s.OutputBufferSize = 1024;
s.Timeout          = pTIMEOUT;

try
    fopen(s);
catch
    disp('[Error] Can''t open COM port. Finishing...');
    return;
end

% Write to FPGA (get buffer)
fwrite(s,   1)

% Read buffer
disp('Data acquisition...');
[rx_buf, cnt, msg] = fread(s, pBUFFER, 'char');
rx_buf = rx_buf.';
disp(['COM port end code: ' msg]);
disp(['Readed ' num2str(cnt) ' bytes']);
disp('Done');

% Close & clear COM port
fclose(s);
delete(s);

save('daq.mat');
