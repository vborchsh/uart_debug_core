
//
// Project       : UART core
// Author        : Borshch Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : uart_rx.sv
// Description   : UART receiver
//

module uart_rx
  #(
    parameter int pT_CLK = 3124
  )
  (
    input              iclk ,
    input              irst ,
    input              irx  , // UART RX line
    //
    output logic       oena = '0 ,
    output logic [7:0] odat = '0
  );

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  logic               [4:0] frx      = '0;
  logic                     true_rx  = '0;
  logic                     dtrue_rx = '0;
  logic                     ena      = '0;
  logic                     clk_ena  = '0;
  logic                     fall_rx  = '0;
  logic                     eop      = '0;
  logic  [$clog2(pT_CLK):0] cnt      = '0;
  logic               [3:0] cnt_pack = '0;
  logic               [7:0] freg     = '0;

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  // Metastable & jitter registers
  always_ff@(posedge iclk)  frx <= frx << 1 | irx;
  always_ff@(posedge iclk) begin
    if (&frx)            true_rx <= 1'b1;
    else if (frx == '0)  true_rx <= 1'b0;
    dtrue_rx <= true_rx;
  end

  // Fronts detection
  always_ff@(posedge iclk) begin
    fall_rx <= dtrue_rx & ~true_rx;
  end

  // Enable signals
  always_ff@(posedge iclk) begin
    if (fall_rx)         ena <= 1'b1;
    else if (eop)        ena <= 1'b0;
    //
    if (irst)            cnt <= '0;
    else if (clk_ena)    cnt <= '0;
      else if (ena)      cnt <= cnt + 1'b1;
    // First clock with shift by T/2 for good signal phase
    clk_ena <= (cnt_pack == 0) ? (cnt == pT_CLK/2-2) : (cnt == pT_CLK-2);
    //
    if (irst)                  cnt_pack <= '0;
    else if (~ena)             cnt_pack <= '0;
      else if (ena & clk_ena)  cnt_pack <= cnt_pack + 1'b1;
    //
    eop <= (cnt_pack == 4'd8) & ena & clk_ena; // START + 8 DATA bytes
  end

  // Data collect
  always_ff@(posedge iclk) begin
    if (clk_ena)  freg <= {true_rx, freg[7:1]};
  end

  //--------------------------------------------------------------------------------------------------
  //
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    oena <= eop;
    odat <= (eop) ? (freg) : (odat);
  end

endmodule
